<?php

namespace Drupal\registration_extras\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\registration_extras\UserFormModeSettingsInterface;

/**
 * Defines the Example entity.
 *
 * @ConfigEntityType(
 *   id = "user_form_mode_settings",
 *   label = @Translation("User Form Mode Settings"),
 *   handlers = {
 *   },
 *   config_prefix = "registration_extras",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   config_export = {
 *     "id",
 *     "submit_button_label",
 *     "destination",
 *     "disable_redirecting_admins",
 *     "form_class",
 *   }
 * )
 */
class UserFormModeSettings extends ConfigEntityBase implements UserFormModeSettingsInterface {

  /**
   * The Example ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Submit Button label.
   *
   * @var string
   */
  protected $submit_button_label;

  /**
   * The form class.
   *
   * @var string
   */
  protected $form_class;

  /**
   * The destination path.
   *
   * @var string
   */
  protected $destination;

  /**
   * Not to redirect admins when they use user creation/registration forms.
   *
   * @var bool
   */
  protected $disable_redirecting_admins;

  public function getSubmitButtonLabel() {
    return $this->submit_button_label;
  }

  public function setSubmitButtonLabel($label) {
    $this->submit_button_label = $label;
  }

  public function getDestination() {
    return $this->destination;
  }

  public function setDestination($destination) {
    $this->destination = $destination;
  }

  public function getDisableRedirectingAdmins() {
    return $this->disable_redirecting_admins;
  }

  public function setDisableRedirectingAdmins($disable_redirecting_admins) {
    $this->disable_redirecting_admins = $disable_redirecting_admins;
  }

  public function getFormClass() {
    return $this->form_class;
  }

  public function setFormClass($form_class) {
    $this->form_class = $form_class;
  }

}

