<?php

namespace Drupal\registration_extras;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Example entity.
 */
interface UserFormModeSettingsInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}

